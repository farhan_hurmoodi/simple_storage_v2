from brownie import accounts, config, SimpleStorage, network


def get_account():
    if network.show_active() == "development":
        return accounts[0]
    else:
        return accounts.add(config["wallets"]["from_key"])


def deploy_simple_storage_contract():
    account = get_account()
    print("Deployer adress: ", account)
    simple_storage = SimpleStorage.deploy({"from": account})
    print("Current people count: ", simple_storage.peopleCount())
    print("Signing...")
    tx = simple_storage.signup("Farhan", 35, {"from": account})
    tx.wait(1)
    print("Current people count: ", simple_storage.peopleCount())
    print("Last singer address: ", simple_storage.efficientAddressFetcher(0))


def main():
    deploy_simple_storage_contract()
