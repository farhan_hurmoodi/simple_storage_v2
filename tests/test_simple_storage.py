from brownie import accounts, config, SimpleStorage


def test_deploy():
    # Arrange
    account = accounts[0]
    # Act
    simple_storage = SimpleStorage.deploy({"from": account})
    actual_people_count = simple_storage.peopleCount()
    expected_people_count = 0
    # Assert
    assert actual_people_count == expected_people_count


def test_signup():
    # Arrange
    account = accounts[0]
    name = "Farhan"
    age = 35
    # Act
    simple_storage = SimpleStorage.deploy({"from": account})
    simple_storage.signup(name, age, {"from": account})
    actual_signer_address = simple_storage.efficientAddressFetcher(0)
    expected_signer_address = account
    # Assert
    assert actual_signer_address == expected_signer_address
